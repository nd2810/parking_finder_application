<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/15/17
 * Time: 9:30 AM
 */

class Parking_Lot
{
    protected static $tableName = "parking_lots";

    protected $title;
    protected $totalCapacity;
    protected $totalHandicapCapacity;
    protected $floors;
    protected $permitRequirements;
    protected $currentAvailabilities;
    protected $currentHandicapAvailabilities;

    // Creates the lot
    public function __construct($title, $totalCapacity, $totalHandicapCapacity,
                                $permitRequirements, $currentAvailabilities = 0,
                                $currentHandicapAvailabilities = 0, $floors = NULL) {
        // Set the instance variables
        $this->title = $title;
        $this->totalCapacity = $totalCapacity;
        $this->totalHandicapCapacity = $totalHandicapCapacity;
        $this->permitRequirements = $permitRequirements;
        $this->currentAvailabilities = $currentAvailabilities;
        $this->currentHandicapAvailabilities = $currentHandicapAvailabilities;

        // Check for null floors parameter
        if (isset($floors)) {
            $this->floors = $floors;
        }
    }

    // Saves the object to the database
    protected function save() {

    }

    // Prints the HTML
    public function __toString()
    {
        // Build the container
        $container = "<div class='parking_lot' onclick='showDetailedLotView();'>";

        // Add the title
        $container .= "<h2>$this->title</h2>";

        // Create an inner container
        $innerContainer = "<div>";

        // Add the important data
        $innerContainer .= "<span id='" . $this->title . "_permit_reqs'>Permit Requirements: " . $this->permitRequirements . "</span><br>";
        $innerContainer .= "<span id='" . $this->title . "_capacity'>Available Parking: " . $this->currentAvailabilities . " / " . $this->totalCapacity . "</span><br>";
        $innerContainer .= "<span id='" . $this->title . "_handicap_capacity'>Available Handicap Parking: " . $this->currentHandicapAvailabilities . " / " . $this->totalHandicapCapacity . "</span><br>";

        // Check for floor data
        if (!isset($this->floors) || sizeof($this->floors) == 0) {
            $innerContainer .= "<span id='" . $this->title . "_floor_count'>Number of Floors: 0</span><br>";
        }
        else {
            $innerContainer .= "<span id='" . $this->title . "_floor_count'>Number of Floors: " . sizeof($this->floors) . "</span><br>";
        }

        // Close the inner container
        $innerContainer .= "</div>";

        // Add the inner container to the outer
        $container .= $innerContainer;

        // Close the container
        $container .= "</div>";

        // Return
        return $container;
    }

    // Gets all of the lots available
    public static function getAllLots() {
        // Create the array to hold all of the lots
        $lots = [];

        // Create a new lot
        // TODO: Load from database
        $lots[] = (new self("Creason Lot", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();
        $lots[] = (new self("Parking Structure One", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();
        $lots[] = (new self("Parking Structure Two", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();
        $lots[] = (new self("Chestnut South Lot", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();
        $lots[] = (new self("Chestnut North Lot", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();
        $lots[] = (new self("University Lot", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();
        $lots[] = (new self("Adams Street Lot", 400, 5, "Non-Premium Parking", 0, 0, NULL))->__toString();

        // Return
        return $lots;
    }

    // Returns an object loaded from the database
    public static function getByID($id) {

    }
}
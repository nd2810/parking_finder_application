<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/21/17
 * Time: 9:53 AM
 */
class Create_Parking_Lot_Form_View
{
    public function __toString() {
        // Return the container
        return $this->getClickout() . $this->getContainer();
    }

    protected function getClickout() {
        // Return the clickout
        return "<div id='create_parking_lot_clickout' onclick='closeCreateParkingLotPopup();'></div>";
    }

    protected function getContainer() {
        // Create the container
        $container = "<div id='create_parking_lot_container'>";

        // Create the name input and label
        $container .= "<label>Parking Lot Name: </label>";
        $container .= "<input type='text' name='lot_name' placeholder='Lot Name'>";
        $container .= "<br><br>";

        // Create the description input and label
        $container .= "<label>Parking Lot Description: </label>";
        $container .= "<textarea name='lot_description' placeholder='Lot Description' maxlength='255'></textarea>";
        $container .= "<br><br>";

        // Create the image input and label
        $container .= "<label>Parking Lot Image: </label>
                        <form action='' enctype='multipart/form-data' method='POST' name='frm_user_file'>
                        <input type='file' name='myfile'/> 
                        <input type='submit' name='submit' value='Upload' onclick='return false;'/>
                        </form><br><br>";

        // Create the closed input and label
        $container .= "<label>Mark as Closed: </label>";
        $container .= "<input type='checkbox' name='lot_closed'/>";
        $container .= "<br><br>";

        // Create the submit button
        $container .= "<button onclick='' class='create_button'>Create</button>";

        // Close it and return it
        return $container . "</div>";
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/23/17
 * Time: 9:27 PM
 */
class Detailed_Parking_Lot_View
{
    public function __construct() {
        //TODO: Get detailed data from separate table in the database
    }

    public function __toString() {
        // Create the container
        $container = "<div class='detailed_parking_lot'>";

        // Close the container
        $container .= "</div>";

        // Create the return button
        $return = "<div id='return_button' onclick='returnToParkingLotList();'>Return</div>";

        // Return the string
        return $container . $return;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: ant45527
 * Date: 2/13/2017
 * Time: 9:46 AM
 */

class Parking_Finder_View
{
    public function __toString() {
        // Return the components as a string
        return $this->getBackdrop() . $this->getBanner(). $this->getContainer() . $this->getFooter();
    }

    protected function getBackdrop() {
        // Create the backdrop
        $backdrop = "<div id='backdrop'><img src='images/wku_background.jpg'></div>";

        // Return it
        return $backdrop;
    }

    protected function getBanner() {
        // Create the banner
        $banner = "<div id='banner'>
                    <img id='wku_logo' src='images/wku_logo.png'>
                    <img id='parking_finder_logo' src='images/parking_finder_logo_banner.png'>
                    <img id='settings_button' onclick='toggleLoginForm();' src='images/cog.png'>" .
                    $this->getLoginForm()
                    . "</div>";

        // Return it
        return $banner;
    }

    protected function getLoginForm() {
        // Create the container
        $container = "<div id='login_form'>";

        // Add a title to the container
        $container .= "<h3>Login to the Admin Control Panel</h3>";

        // Add the login label and input
        $container .= "<label>Username: </label>";
        $container .= "<input type='text' name='username'><br>";

        // Add the password label and input
        $container .= "<label>Password: </label>";
        $container .= "<input type='password' name='password'><br>";

        // Add the error message container
        $container .= "<div class='error_message'></div>";

        // Add the submit
        $container .= "<button onclick='login();'>Login</button>";

        // Return it
        return $container;
    }

    protected function getContainer() {
        // Create the container
        $container = "<div id='parking_lot_list'>";

        // Get the parking lots
        // TODO: Load parking lots from database (rather than use hard-coded)
        $parkingLots = Parking_Lot::getAllLots();

        // Add the parking lots to the container
        $container .= implode($parkingLots);

        // Close the container
        $container .= "</div>";

        // Return the container
        return $container;
    }

    protected function getFooter() {
        // Create the footer
        $footer = "<div id='footer'><span>&copy Copyright Dao, Netthisinghe, Norman, and Renaud 2017</span></div>";

        // Return it
        return $footer;
    }
}
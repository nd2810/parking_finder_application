<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/26/17
 * Time: 9:04 PM
 */

class Control_Panel_View
{
    protected $userType; // 0 for mod, 1 for admin

    public function __construct() {
        // Set this user type to reflect the requested view type
        $this->userType = $_SESSION["user_type"];
    }

    public function __toString() {
        // Return the container
        return $this->getClickout() . $this->getContainer();
    }

    protected function getClickout() {
        // Return the clickout
        return "<div id='control_panel_clickout' onclick='closeControlPanel();'></div>";
    }

    protected function getContainer() {
        // Create the container
        $container = "<div id='control_panel_container'>";

        // Check if admin
        if ($this->userType == 1) {
            // Add the new parking lot button
            $container .= "<button class='create_button' onclick='openNewParkingLotPopup();'>+ New Parking Lot</button>";
        }

        // TODO: Build the tables for already present parking lots

        // Close it and return it
        return $container . "</div>";
    }
}
/**
 * Created by ant45527 on 2/13/2017.
 */

// Closes the create parking lot popup
function closeCreateParkingLotPopup() {
    $("#create_parking_lot_container").fadeOut(300, function() {
        $(this).remove();
    });
    $("#create_parking_lot_clickout").fadeOut(300, function() {
        $(this).remove();
    });
}

// Opens the popup for creating a new parking lot
function openNewParkingLotPopup() {
    // Request access to the new parking lot popup
    $.ajax({
        url: 'ajax/open_new_parking_lot_popup.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for successful login
            if (jData["success"] == true) {
                // Append the view
                $(jData["toAppend"]).appendTo("body").fadeIn(300);
            }
            else {
                // Inform of inadequate privileges
                alert("You do not have the proper permissions to perform this action!");
            }
        },
        error: function() {
            console.log("Error with logging into the control panel!");
        }
    });
}

// Submits the login credentials and attempts to log the user in
function login() {
    // Determine the provided credentials
    var username = $("#login_form input[name='username']");
    var password = $("#login_form input[name='password']");

    // Request access to the control panel view
    $.ajax({
        url: 'ajax/login_to_control_panel.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            username: username.val(),
            password: password.val()
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for successful login
            if (jData["success"] == true) {
                // Clear the password box
                $(password).val("");

                // Close the login form
                toggleLoginForm();

                // Append the view
                $(jData["toAppend"]).appendTo("body").fadeIn(300);
            }
            else {
                // Indicate bad login credentials
                $(username).css("border", "1px solid red");
                $(password).css("border", "1px solid red");
                $("#login_form .error_message").text("Invalid credentials.");
            }

        },
        error: function() {
            console.log("Error with logging into the control panel!");
        }
    });
}


// Closes the control panel
function closeControlPanel() {
    // Request the corresponding detailed view
    $.ajax({
        url: 'ajax/logout.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function() {
            // Close the control panel
            $("#control_panel_container").fadeOut(300, function() {
                $(this).remove();
            });
            $("#control_panel_clickout").fadeOut(300, function() {
                $(this).remove();
            });
        },
        error: function() {
            console.log("Error with saving secret!");
        }
    });
}


// Shows/hides the login form
function toggleLoginForm() {
    $("#login_form").slideToggle(300);
}

// Return to the main list
function returnToParkingLotList() {
    // Fade out the detailed view and remove it
    $(".detailed_parking_lot").fadeOut(300, function() {
        $(this).remove();
    });
    // Fade out the return button and remove it
    $("#return_button").fadeOut(300, function() {
        $(this).remove();

        // Fade in the parking lot list
        $("#parking_lot_list").fadeIn(300);
    });
}

// Shows the detailed view for the selected lot
function showDetailedLotView() {
    // Request the corresponding detailed view
    $.ajax({
        url: 'ajax/get_detailed_lot_view.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Append the detailed view
            $("body").append(jData["detailed_view"]);

            // Fade out the parking lot list
            $("#parking_lot_list").fadeOut(300, function() {
                // Fade in the view
                $(".detailed_parking_lot").fadeIn(300);
                // Fade in the return button
                $("#return_button").fadeIn(300);
            });

        },
        error: function() {
            console.log("Error with showing detailed lot view!");
        }
    });
}
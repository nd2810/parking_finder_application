-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 20, 2017 at 03:46 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wku_parking_finder`
--
CREATE DATABASE IF NOT EXISTS `wku_parking_finder` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `wku_parking_finder`;

-- --------------------------------------------------------

--
-- Table structure for table `parking_lots`
--

CREATE TABLE `parking_lots` (
  `lot_id` int(8) NOT NULL,
  `lot_name` varchar(30) NOT NULL,
  `lot_desc` varchar(255) NOT NULL,
  `lot_pic` blob NOT NULL,
  `display_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parking_lot_floors`
--

CREATE TABLE `parking_lot_floors` (
  `lot_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `permit_id` int(11) NOT NULL,
  `floor_avail` int(10) NOT NULL COMMENT 'Floor availability',
  `floor_message` varchar(256) NOT NULL,
  `total_spots` int(255) NOT NULL,
  `total_handicap_spots` int(255) NOT NULL,
  `available_spots` int(255) NOT NULL,
  `available_handicap_spots` int(255) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `display_status` tinyint(1) NOT NULL COMMENT '1 to enable floor, 0 to disable/delete floor'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parking_permits`
--

CREATE TABLE `parking_permits` (
  `permit_id` int(64) NOT NULL,
  `permit_name` varchar(128) NOT NULL,
  `permit_status` tinyint(1) NOT NULL COMMENT '1 to enable permit type, 0 to disable permit type'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `user_type` tinyint(1) NOT NULL COMMENT 'denotes the user type, super-admin or mod',
  `user_status` tinyint(1) NOT NULL COMMENT '1 means enabled, 0 means disabled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Admin & mod user account info';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `password`, `user_type`, `user_status`) VALUES
('pntadmin', 'be387f089d0da0f8836504ec0193b227270293dd', 1, 1),
('pntmod', '6b03c03e67e9ccae3371012d42d2c4932071110a', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parking_lots`
--
ALTER TABLE `parking_lots`
  ADD PRIMARY KEY (`lot_id`);

--
-- Indexes for table `parking_lot_floors`
--
ALTER TABLE `parking_lot_floors`
  ADD PRIMARY KEY (`floor_id`);

--
-- Indexes for table `parking_permits`
--
ALTER TABLE `parking_permits`
  ADD PRIMARY KEY (`permit_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parking_lots`
--
ALTER TABLE `parking_lots`
  MODIFY `lot_id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parking_lot_floors`
--
ALTER TABLE `parking_lot_floors`
  MODIFY `floor_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
